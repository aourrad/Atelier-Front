import coordonates from "./json/coordinates.json";

export class Logic {
    constructeur() {
        this.gapX = 0;
        this.index = 0;
        this.player1 = true;
        this.player2 = true;
        this.vitesseBalle = 1;
    }

    /**
     * Méthode random nombres 
     */
    getRandom(min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    }

    /**
     * Calcul de la trajectoir de la balle
     *  pour les coordonnées X
     */
    calcGapX(positionNow, destination) {
        if (destination - positionNow) {
            this.gapX = (destination - positionNow) / 46;
        } else {
            this.gapX = this.getRandom(-2, 2);
        }
    }

    positionBalleX(balle) {
        return balle.positionX + this.gapX;
    }

    /**
     * Calcul de la trajectoir de la balle
     *  pour les coordonnées Y
     */
    positionBalleY(balle) {
        if (balle.positionY <= 260) {
            this.steepBalle = 10;
        } else if (balle.positionY >= 720) {
            this.steepBalle = -10;
        }

        return (balle.positionY += this.steepBalle);
    }

    /**
     * Gestion du mouvement du joueur adverse
     */
    movePlayer2(positionX, destination) {
        if (destination - positionX > this.getRandom(2, 10)) {
            return this.getRandom(2, 10);
        } else {
            return this.getRandom(2, 10) * -1;
        }
    }
}
