"use strict";

import { Game } from "./game";
import library from "./library";
import Cv from "../views/cv.pug";
import Nav from "../views/nav.pug";
import Accueil from "../views/accueil.pug";
import CanvasTemplate from "../views/jeu.pug";

const navigation = {
    /**
     * Le menu de navigation
     */
    nav: {
        menu: [
            { name: "Accueil", link: "accueil", active: "true" },
            { name: "Cv", link: "cv" },
            { name: "Jeu", link: "jeu" }
        ]
    },
    /**
     * Initialisation de la partie
     */
    initGame: function() {
        this.game = new Game();
        return this;
    },
    /**
     * Injection dans le DOM  ( Menu de navigation )
     */
    viewInNav: function() {
        document.querySelector("nav").innerHTML = Nav(this.nav);
        return this;
    },
    /**
     * Injection dans le DOM  ( sections acceuil/CV/jeu )
     */
    templateDom: page => {
        document.querySelector("main").innerHTML = page;
        return this;
    },

    viewInMain: function(key) {
        switch (key) {
            case "accueil":
                this.templateDom(Accueil());
                library.animationHome(0, 0, "0s");
                break;

            case "cv":
                this.templateDom(Cv());
                break;

            case "jeu":
                this.templateDom(CanvasTemplate());
                this.initGame();
                this.game.startGame();
                break;

            default:
                this.templateDom(Accueil());

                break;
        }
        return this;
    }
};

export default navigation;
