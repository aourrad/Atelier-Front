"use strict";

import { Commandes } from "./script/commandes";
import navigation from "./script/navigation";
import library from "./script/library";
import "bootstrap/scss/bootstrap.scss";
import "./styles/style.scss";

const commandes = new Commandes();

/**
 * Injection dans le DOM le menu de navigation
 */
navigation.viewInNav().viewInMain();

window.onload = function(e) {
    /**
     * Background-image et animation page d'accueil
     */
    library.animationHome(2000, 3000, "2s");

    /**
     * Affichage (instructions jeu) ou (canvas) dans la page Jeu
     */
    const toggleCanvasReglage = function() {
        if (commandes.isMobile().any()) {
            library.audioOff();
        }

        library.toggleInstructionCanvas(navigation);
    };

    /**
     * Evénements au clic dans le menu de navigation
     */
    const navEltLink = document.querySelectorAll(".nav-link");

    for (let i = 0; i < navEltLink.length; i++) {
        navEltLink[i].addEventListener("click", function(evt) {
            for (let j = 0; j < navEltLink.length; j++) {
                navEltLink[j].classList.remove("is-active", "disabled");
            }
            this.classList.add("is-active", "disabled");
            evt.preventDefault();
            navigation.viewInMain(this.getAttribute("data-pathname"));
            toggleCanvasReglage();
        });
    }

    /**
     * Chargement de script jquery mobile si Mobile
     */
    if (commandes.isMobile().any()) {
        const scripMobile = document.createElement("script");
        scripMobile.src =
            "http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js";
           
        document.body.appendChild(scripMobile);
    }
    
};

