"use strict"

const library = {
    /**
     * Réglage de la partie
     */
    reglages: function(gameContext) {
        $('input:checkbox[name="audio"]').change(
            function(evt) {
                console.log(gameContext);
                gameContext.audio = evt.target.checked;
            }.bind(gameContext)
        );

        $('input:radio[name="niveau"]').change(
            function(evt) {
                gameContext.niveau = parseInt(evt.target.value);
            }.bind(gameContext)
        );

        $('input:radio[name="sets"]').change(
            function(evt) {
                gameContext.nbrSets = parseInt(evt.target.value);
            }.bind(gameContext)
        );
    },
    /**
     * Service et déplacement de joueur sur écrants tactiles
     */
    mobileEvent: function(gameContext) {
        $(document).on(
            "swiperight",
            gameContext.mobileService.bind(gameContext)
        );
        $("canvas").on(
            "click",
            function(evt) {
                gameContext.player1.playerPosition.x =
                    (evt.offsetX * 1400) / evt.target.offsetWidth - 60;
            }.bind(gameContext)
        );
    },
    /**
     * Désactiver le son
     */
    audioOff: function() {
        $("#audio").css("display", "none");
    },
    /**
     * Affichage (instructions jeu) ou (canvas)
     */
    toggleInstructionCanvas: function(navigationContext) {

        $("#on").on("click", function() {
            $("canvas").fadeIn("slow");
            $(".instructions").fadeOut("slow");
            navigationContext.game.initCanvas();
            navigationContext.game.stop = false;
        });

        $("#stop-game").on("click", function() {
            $("canvas").fadeOut("slows");
            $(".instructions").fadeIn("slow");
            navigationContext.game.stop = true;
            navigationContext.initGame();
            navigationContext.game.startGame();
        });
    },
    /**
     * Background-image et animation page d'accueil
     */
    animationHome: function(animation1, animation2, transition) {
        $(document).ready(function() {
            $("body").css(
                "background-image",
                "url('assets/img/ball-game.jpg')"
            );
            setTimeout(() => {
                $(".filet")
                    .css("transition", transition + " ease-in-out")
                    .addClass("presentation");
            }, animation1);

            setTimeout(() => {
                $(".separation div")
                    .css("transition", transition + " ease-in-out")
                    .css("opacity", 1);
            }, animation2);
        });

        return this;
    }
};

export default library;
