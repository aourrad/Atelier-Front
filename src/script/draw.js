export class Draw {
    constructor(ctx) {
        this.imgEltMedal = new Image();
        this.imgEltPlayers = new Image();
        /**
         * INIT sprites
         */
        this.imgEltMedal.src = "./assets/img/medal.png";
        this.imgEltPlayers.src = "./assets/img/ping.png";
    }
    /**
     * Générateur de gradients
     */
    gradient(ctx, gradientP, colors) {
        const gradient = ctx.createLinearGradient(
            gradientP.x0,
            gradientP.y0,
            gradientP.x1,
            gradientP.y1
        );
        for (let i = 0; i < colors.length; i++) {
            let index = i;
            if (colors.length === 1) index = 1;
            else index = (i / 10) * (10 / (colors.length - 1));
            gradient.addColorStop(index, colors[i]);
        }
        return gradient;
    }

    /**
     * Méthodes pour dessiner dans canvas
     */

    line(ctx, coordonates, color) {
        ctx.beginPath();
        ctx.strokeStyle = color;
        ctx.lineWidth = coordonates.width;
        ctx.lineCap = "round";
        ctx.moveTo(coordonates.x0, coordonates.y0);
        ctx.lineTo(coordonates.x1, coordonates.y1);
        ctx.fill();
        ctx.stroke();
    }

    rectangle(ctx, x, y, w, h, gradientP, colors) {
        ctx.fillStyle = this.gradient(ctx, gradientP, colors);
        ctx.beginPath();
        ctx.fillRect(x, y, w, h);
        ctx.fill();
    }

    custom(ctx, arrCoordonates, gradientP, colors, border = false) {
        ctx.fillStyle = this.gradient(ctx, gradientP, colors);
        ctx.strokeStyle = border.color;
        ctx.lineWidth = border.width;
        ctx.beginPath();
        ctx.moveTo(arrCoordonates[0].x, arrCoordonates[0].y);
        for (let i = 1; i < arrCoordonates.length; i++) {
            ctx.lineTo(arrCoordonates[i].x, arrCoordonates[i].y);
        }
        ctx.fill();
        if (!border) ctx.stroke();
    }
    /**
     * Affichage score 1
     */
    scorePlayer1(ctx, points, sets) {
        let nbr = 0;
        if (points >= 10) nbr = 5;
        ctx.transform(1, -1, 0, 1, 0, 0);
        ctx.font = "25px Arial";
        ctx.fillStyle = nbr ? "#BF0404" : "#FFF";
        ctx.fillText(points, 98 - nbr, 598 + nbr);
        ctx.font = "12px Arial";
        ctx.fillStyle = "white";
        ctx.fillText(sets, 104, 570);
    }
    /**
     * Affichage score 2
     */
    scorePlayer2(ctx, points, sets) {
        let nbr = 0;
        if (points >= 10) nbr = 5;
        ctx.font = "22px Arial";
        ctx.fillStyle = nbr ? "#BF0404" : "#FFF";
        ctx.fillText(points, 134 - nbr, 600);
        ctx.font = "12px Arial";
        ctx.fillStyle = "white";
        ctx.fillText(sets, 122, 575);
    }
    /**
     * Affichage des raquettes
     */
    runGame(
        ctx,
        gradient,
        colors,
        positionPlayer1,
        positionPlayer2,
        opacity,
        positionBalle
    ) {
        ctx.transform(1, 1, 0, 1, 0, 0);
        ctx.drawImage(
            this.imgEltPlayers,
            260,
            46,
            240,
            405,
            positionPlayer2,
            206,
            74,
            124
        );
        this.balle(ctx, gradient, colors, positionBalle, opacity);
        ctx.drawImage(
            this.imgEltPlayers,
            0,
            50,
            240,
            405,
            positionPlayer1,
            660,
            120,
            202
        );
    }

    balle(ctx, gradientP, colors, position, opacity) {
        ctx.beginPath();
        ctx.fillStyle = this.gradient(ctx, gradientP, colors);
        ctx.arc(position.x, position.y, position.w, 0, 2 * Math.PI * opacity);
        ctx.fill();
        if (opacity) ctx.stroke();
    }

    competences(ctx, arrCompetences, nbr, sets) {
        const arrLength = sets ? arrCompetences.length : nbr;
        for (let i = 0; i < arrLength; i++) {
            ctx.drawImage(
                this.imgEltMedal,
                arrCompetences[i].x,
                arrCompetences[i].y,
                arrCompetences[i].w,
                arrCompetences[i].h,
                arrCompetences[i].positionX + 23,
                105,
                50,
                80
            );
        }
    }

    text(ctx, text, color) {
        ctx.fillStyle = color;
        ctx.font = "100px VT323";
        ctx.fillText(text, 180, 450);
    }
}
