const HTMLplugin = require("html-webpack-plugin");
const DashboardPlugin = require("webpack-dashboard/plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const DevMode = process.env.npm_lifecycle_script.indexOf("dev") > 0;
const path = require("path");

module.exports = {
    devServer: {
        // host :,
        // port :,
        contentBase: path.resolve(__dirname, "dist")
    },
    module: {
        rules: [
            {
                test: /\.m?js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: [
                            [
                                "@babel/preset-env",
                                {
                                    targets: {
                                        chrome: "58",
                                        ie: "11"
                                    }
                                }
                            ]
                        ]
                    }
                }
            },
            {
                test: /\.css$/,
                use: [
                    DevMode ? "style-loader" : MiniCssExtractPlugin.loader,
                    {
                        loader: "css-loader",
                        options: {
                            importLoaders: 1,
                            sourceMap: true,
                            minimize: true,
                            colormin: false
                        }
                    }
                ]
            },
            {
                test: /\.s[ac]ss$/i,
                use: DevMode
                    ? [
                          "style-loader",
                          {
                              loader: "css-loader",
                              options: { sourceMap: true }
                          },
                          {
                              loader: "sass-loader",
                              options: { sourceMap: true }
                          }
                      ]
                    : [
                          {
                              loader: MiniCssExtractPlugin.loader,
                              options: {
                                  hmr: false
                              }
                          },
                          "css-loader",
                          "postcss-loader",
                          "sass-loader"
                      ]
            },
            {
                test: /\.(html)$/,
                use: {
                    loader: "html-loader",
                    options: {
                        attrs: [":data-src"]
                    }
                }
            },
            {
                test: /\.pug$/,
                use: ["pug-loader"]
            },
            {
                test: /\.(png|jpe?g|gif|svg|ico)(\?.*)?$/,
                use: {
                    loader: "url-loader",
                    options: {
                        limit: 8000,
                        name: "[name].[hash:7].[ext]",
                        outputPath: "imgs"
                    }
                }
            }
        ]
    },

    devtool: "cheap-module-eval-source-map",

    plugins: [
        new HTMLplugin(),
        new DashboardPlugin(),
        new HtmlWebpackPlugin({
            filename: "index.html",
            template: "./src/views/index.pug",
            inject: true
        }),
        new MiniCssExtractPlugin({
            filename: !DevMode ? "[name].css" : "[name].[hash].css",
            chunkFilename: DevMode ? "[id].css" : "[id].[hash].css"
        })
    ]
};
