import coordinates from "./json/coordinates.json";
import competences from "./json/competences.json";
import { Players, Balle } from "./players";
import { Commandes } from "./commandes";
import library from "./library";
import { Logic } from "./logic";
import { Draw } from "./draw";

export class Game extends Commandes {
    constructor() {
        super();
        this.niveau = 0;
        this.stop = true;
        this.nbrSets = 1;
        this.vitesse = 16;
        this.audio = true;
        this.incrementP1 = 0;
        this.incrementP2 = 0;
        this.runGame = false;
        this.draw = new Draw();
        this.logic = new Logic();
        this.balle = new Balle();
        this.player1 = new Players(coordinates.player1);
        this.player2 = new Players(coordinates.player2);
    }

    /**
     * Initialisation de jeu
     */
    initCanvas() {
        this.canvas = document.getElementById("canvas");
        this.ctx = this.canvas.getContext("2d");
        this.fullScreenCanvas()
            .keyCommand()
            .runAnimation(this.draw, this.ctx);
    }

    /**
     * Initialiosation des événements
     */
    keyCommand() {
        if (this.isMobile().any()) {
            library.mobileEvent(this);
        } else {
            window.addEventListener("keydown", this.keyDown.bind(this));
            window.addEventListener("keyup", this.keyUp.bind(this));
        }
        return this;
    }

    /**
     * Initialisation de la partie
     */

    startGame() {
        library.reglages(this);
        this.audioOut = new Audio("./assets/audio/out.mp3");

        /**
         * 1er Service
         */
        this.player1.service = this.logic.getRandom(0, 2) ? true : false;
    }

    /**
     * Démarrage de l'animation
     */
    runAnimation(draw, ctx) {
        window.requestAnimationFrame =
            window.requestAnimationFrame ||
            window.mozRequestAnimationFrame ||
            window.webkitRequestAnimationFrame ||
            window.msRequestAnimationFrame;
        let timestampPrev = 0;
        const animationFrame = timestamp => {
            if (timestamp - timestampPrev >= this.vitesse) {
                timestampPrev = timestamp;
                this.ctx.clearRect(0, 0, 1400, 860);
                this.drawEltCanvas(draw, ctx)
                    .runGameOn()
                    .score(draw, ctx)
                    .movePlayer1(draw, ctx, this.balle.position())
                    .movePlayer2()
                    .finPartie(draw, ctx);
            }

            if (!this.stop) {
                window.requestAnimationFrame(animationFrame);
            }
        };

        window.requestAnimationFrame(animationFrame);

        return this;
    }

    /**
     * Méthode qui gére les Services de balle
     */
    toggleService() {
        if ((this.player1.score + this.player2.score) % 2 === 0) {
            this.player1.service = !this.player1.service;
        }
        this.player2.service = !this.player1.service;
        this.balle.initPosition(this.player1.service);
        this.player1.playerPosition.x = 660;
        this.player2.playerPosition.x = 663;
    }

    /**
     * Méthode qui gére le mouvement de la balle
     */
    runBalle(condition, playerA, playerB, coef) {
        if (condition) {
            this.player1.audio = new Audio("./assets/audio/p1.mp3");
            this.player2.audio = new Audio("./assets/audio/p2.mp3");
            if (this.niveau) this.vitesse = this.logic.getRandom(14, 17);
            else this.vitesse = this.logic.getRandom(17, 19);
            if (
                this.balle.positionX > playerA.playerPosition.x &&
                this.balle.positionX <
                    playerA.playerPosition.x + playerA.playerInit.w
            ) {
                const destination =
                    (playerA.playerPosition.x +
                        playerA.playerInit.w / 2 -
                        this.balle.positionX) *
                        coef +
                    700;
                this.logic.calcGapX(this.balle.positionX, destination);
                if (this.balle.positionY < 730 && this.audio)
                    playerA.audio.play();

                return destination;
            } else {
                if (playerB.wine()) playerA.score = 0;
                this.runGame = false;
                this.balle.balleOnService = false;
                if (!this.isMobile().any() && this.audio) this.audioOut.play();
            }
        }
    }

    /**
     * Le mouvement de joueur 1
     */
    movePlayer1(draw, ctx, ballePosition) {
        draw.runGame(
            ctx,
            coordinates.gradientCenter,
            ["#DDD", "#FFF"],
            this.player1.playerPosition.x,
            this.player2.playerPosition.x,
            this.balle.opacity,
            ballePosition
        );
        if (!this.isMobile().any()) {
            this.player1.playerPosition.x += this.incrementP1;
            if (
                this.player1.playerPosition.x < 200 ||
                this.player1.playerPosition.x > 1070
            ) {
                this.incrementP1 = 0;
            }
        }
        return this;
    }

    /**
     * Le mouvement de joueur 2 (EN FACE)
     */
    movePlayer2() {
        if (
            this.player2.playerPosition.x < 500 ||
            this.player2.playerPosition.x > 830 ||
            Math.abs(
                this.player2.playerPosition.x +
                    this.logic.getRandom(20, 54) -
                    this.contact
            ) < 20
        ) {
            this.incrementP2 = 0;
        } else {
            this.player2.playerPosition.x += this.incrementP2;
        }
        return this;
    }

    /**
     * Gestion de la trajectoire de la balle
     */
    runGameOn() {
        if (this.runGame) {
            this.balle.positionX = this.logic.positionBalleX(this.balle);

            this.balle.positionY = this.logic.positionBalleY(this.balle);

            this.translatePlayer1 = this.runBalle(
                this.balle.positionY >= 720,
                this.player1,
                this.player2,
                3.5
            );
            this.translatePlayer2 = this.runBalle(
                this.balle.positionY <= 260,
                this.player2,
                this.player1,
                9
            );

            this.translate();
        }
        return this;
    }

    /**
     *  La position de balle chez l'adversaire
     */
    translate() {
        if (this.translatePlayer1 || this.service) {
            this.incrementP2 = this.logic.movePlayer2(
                this.player2.playerPosition.x,
                this.translatePlayer1
            );

            this.contact = this.translatePlayer1;
        }

        if (this.translatePlayer2) {
            this.contact = this.translatePlayer2;
        }
        return this;
    }

    /**
     * Affichage des scores
     */
    score(draw, ctx) {
        draw.scorePlayer1(ctx, this.player1.score, this.player1.sets);
        draw.scorePlayer2(ctx, this.player2.score, this.player2.sets);
        return this;
    }

    finPartie(draw, ctx) {
        if (this.nbrSets === this.player1.sets) {
            this.stop = true;
            this.drawEltFinPartie(
                draw,
                ctx,
                "Bravo! vous avez gagné",
                "#F5C950"
            );
        } else if (this.nbrSets === this.player2.sets) {
            this.stop = true;
            this.drawEltFinPartie(draw, ctx, "Vous avez perdu", "#A61F38");
        }
    }

    /**
     * Générer le rendu de la scène de jeu
     */
    drawEltCanvas(draw, ctx) {
        draw.rectangle(ctx, 0, 0, 1400, 860, coordinates.gradientCenter, [
            "#FFFFFF",
            "#ABF0FF",
            "#A2E4F2"
        ]);
        draw.rectangle(ctx, 247, 0, 874, 430, coordinates.gradientTop, [
            "#FFFFFF",
            "#ABF0FF",
            "#A2E4F2"
        ]);
        draw.rectangle(ctx, 400, 100, 600, 5, coordinates.gradientTop, [
            "#26448C"
        ]);
        draw.custom(
            ctx,
            coordinates.bordure,
            coordinates.gradientCenter,
            ["#FFFFFF", "#000000"],
            coordinates.borderTable
        );
        draw.custom(
            ctx,
            coordinates.sol,
            coordinates.gradientCenter,
            ["#FFFFFF", "#F49466"],
            coordinates.borderTable
        );
        /* Table */
        draw.custom(ctx, coordinates.table, coordinates.gradientCenter, [
            "#66A3F2",
            "#0583F2"
        ]);
        draw.line(ctx, coordinates.lineVertical, "#FFF");
        /* Filet */
        draw.rectangle(ctx, 410, 490, 578, 40, coordinates.gradientTop, [
            "rgba(255, 255, 255, 0.6)"
        ]);
        draw.line(ctx, coordinates.lineCrochetLeft, "#555");
        draw.line(ctx, coordinates.lineCrochetRight, "#555");
        /* Support score  */
        draw.rectangle(ctx, 40, 520, 70, 132, coordinates.gradientLeft, [
            "#35392F",
            "#FFFFFF"
        ]);
        draw.custom(
            ctx,
            coordinates.supportFace,
            coordinates.gradientCenter,
            ["#35392F", "#FFFFFF"],
            coordinates.border0
        );
        draw.custom(
            ctx,
            coordinates.supportTop,
            coordinates.gradientRight,
            ["#35392F", "#FFFFFF"],
            coordinates.border0
        );
        draw.custom(
            ctx,
            coordinates.score1,
            coordinates.gradientRight,
            ["#306073"],
            coordinates.border0
        );
        draw.custom(
            ctx,
            coordinates.score2,
            coordinates.gradientRight,
            ["#3C7DA6"],
            coordinates.border0
        );
        /* Etagère  */
        draw.line(ctx, coordinates.lineScore, ["#FFFFFF"]);
        /* Compétences  */
        draw.competences(
            ctx,
            competences.elts,
            this.player1.score,
            this.player1.sets
        );
        return this;
    }
    drawEltFinPartie(draw, ctx, text, color) {
        this.ctx.clearRect(0, 0, 1400, 860);
        draw.rectangle(ctx, 0, 0, 1400, 860, coordinates.gradientCenter, [
            "rgba(255, 255, 255, 0.467)",
            "#ABF0FF",
            "rgba(162, 228, 242, 0.6)"
        ]);
        draw.text(ctx, text, color);
    }

    fullScreenCanvas() {
        /**
         * Mode plein écran
         */
        this.canvas.addEventListener("click", function() {
            if (this.requestFullscreen) {
                this.requestFullscreen();
            } else if (this.webkitRequestFullscreen) {
                /* Chrome, Safari and Opera */
                this.webkitRequestFullscreen();
            } else if (this.mozRequestFullScreen) {
                /* Firefox */
                this.mozRequestFullScreen();
            } else if (this.msRequestFullscreen) {
                /* IE/Edge */
                this.msRequestFullscreen();
            }
        });

        this.canvas.addEventListener("dblclick", function() {
            /**
             * sortir du mode plein écran
             */
            if (document.exitFullscreen) {
                document.exitFullscreen();
            } else if (document.mozCancelFullScreen) {
                /* Firefox */
                document.mozCancelFullScreen();
            } else if (document.webkitExitFullscreen) {
                /* Chrome, Safari and Opera */
                document.webkitExitFullscreen();
            } else if (document.msExitFullscreen) {
                /* IE/Edge */
                document.msExitFullscreen();
            }
        });
        return this;
    }
}
