export class Commandes {
    keyDown(evt) {
        switch (evt.keyCode) {
            /**
             * Touches de clavier pour le déplacement du joueur gauche / droite
             */
            case 39:
                if (this.player1.playerPosition.x < 1070) this.incrementP1 = 10;
                break;
            case 37:
                if (this.player1.playerPosition.x > 200) this.incrementP1 = -10;
                break;
            case 32:
                /**
                 *Touches Clavier pour  commencer une partie ou servire
                 */
                this.toggleService();
                this.logic.gapX = 0;
                setTimeout(() => {
                    this.runGame = true;
                }, 1000);

                break;
            default:
                break;
        }
    }
    keyUp(evt) {
        /**
         * Stop le déplacement du joueur
         */
        switch (evt.keyCode) {
            case 39:
            case 37:
                this.incrementP1 = 0;
                break;
            default:
                break;
        }
    }

    /**
     * Service sur écrants tactiles
     */
    mobileService() {
        this.toggleService();
        this.logic.gapX = 0;
        setTimeout(() => {
            this.runGame = true;
        }, 1000);
    }

    isMobile() {
        return {
            Android: function() {
                return navigator.userAgent.match(/Android/i);
            },
            BlackBerry: function() {
                return navigator.userAgent.match(/BlackBerry/i);
            },
            iOS: function() {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i);
            },
            Opera: function() {
                return navigator.userAgent.match(/Opera Mini/i);
            },
            Windows: function() {
                return (
                    navigator.userAgent.match(/IEMobile/i) ||
                    navigator.userAgent.match(/WPDesktop/i)
                );
            },
            any: function() {
                return (
                    this.Android() ||
                    this.BlackBerry() ||
                    this.iOS() ||
                    this.Opera() ||
                    this.Windows()
                );
            }
        };
    }
}
