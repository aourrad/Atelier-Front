/**
 * Constructeur Joueurs
 */

export class Players {
    constructor(player) {
        this.sets = 0;
        this.score = 0;
        this.service = false;
        this.playerInit = player;
        this.playerPosition = { x: 660, y: 0, w: 0 };
    }
    /**
     * Gestion des scores
     */
    wine() {
        this.score++;
        if (this.score === 11) {
            this.score = 0;
            this.sets++;
            return true;
        }
    }
}

/**
 * Constructeur Balle
 */

export class Balle {
    constructor() {
        this.balleOnService = true;
    }

    initPosition(player1) {
        this.positionX = player1 ? 700 : 700;
        this.positionY = player1 ? 840 : 260;
    }
    /**
     * Calcul la taille de la balle pour la profondeur de champ
     */
    calcWidth(position) {
        if (
            (this.positionY <= 260 || this.positionY >= 720) &&
            this.balleOnService
        ) {
            this.w = 5 + (this.positionY - 260) * (5 / 460);
        } else {
            this.w = 5 + (this.positionY - 260) * (5 / 460);
            this.opacity = 1;
        }
    }
    /**
     * Calcul la position de balle
     */
    position(position) {
        this.calcWidth(position);
        return {
            x: this.positionX,
            y: this.positionY,
            w: this.w
        };
    }
}
