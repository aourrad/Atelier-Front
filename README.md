## Atelier front

Je suis développeur FullStack JS junior. Dans le cadre de l'atelier front end de ma formation à l'IFOCOP Paris 11, j'ai conçu ce jeu afin de vous présenter de manière ludique mon CV. Je vous propose, à travers une folle partie de ping-pong, de découvrir mes compétences au fur et à mesure que vous gagnerez des points.

Pour jouer voici le [lien](http://bit.ly/2N4Kl1Y), rendez-vous à l'onglet **Jeu** où vous pourrez choisir un niveau de difficulté et le nombre de sets gagnants.
